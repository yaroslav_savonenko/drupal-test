<?php

/**
 * @file
 * Contains \Drupal\cvs\Form\Multistep\CvsForm.
 */

namespace Drupal\cvs\Form\Multistep;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Date;
use Drupal\Core\Render\Markup;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Locale;

class CvsForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'cv_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($form_state->has('page_num') && $form_state->get('page_num') == 2) {
      return self::cvPageTwo($form, $form_state);
    }

    if ($form_state->has('page_num') && $form_state->get('page_num') == 3) {
      return self::cvPageThree($form, $form_state);
    }

    if ($form_state->has('page_num') && $form_state->get('page_num') == 4) {
      return self::cvPageFour($form, $form_state);
    }

    $form_state->set('page_num', 1);

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    );

    $form['surname'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Surname'),
    );

    $form['birthday'] = array(
      '#type' => 'date',
      '#title' => $this->t('Your BD'),
      '#date_input_format' => 'd.m.Y',
    );

    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['image_photo'] = array(
      '#type' => 'managed_file',
      '#title' => $this->t('Photo image'),
      '#upload_location' => 'public://images/cv-image/',
      '#upload_validators' => [
        'file_validate_extensions' => array('png gif jpg jpeg'),
        'file_validate_size' => array(25600000),
      ],
    );

    $form['contact_info'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'contact-info'),
    );

    $countries = Locale\CountryManager::getStandardList();
    $form['contact_info']['country'] = array(
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $countries,
      '#empty_option' => $this->t('-select-'),
    );

    $form['contact_info']['city'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => $form_state->getValue('city', NULL),
      '#states' => [
        'invisible' => [
          ':input[name="country"]' => ['value' => ''],
        ],
      ],
    );

    $form['contact_info']['address'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#states' => [
        'invisible' => [
          ':input[name="country"]' => ['value' => ''],
        ],
      ],
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#submit' => ['::nextStep'],
    );

    return $form;

  }

  /**
   * Form constructor for the second step of multistep form cv_form.
   */
  public function cvPageTwo(array &$form, FormStateInterface $form_state)
  {
    $form['skills'] = array(
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Enter your skills (comma-separated)'),
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => array('tags'),
      ],
      '#tags' => TRUE,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#submit' => ['::nextStep'],
    );

    return $form;
  }

  /**
   * Form constructor for the third step of multistep form cv_form.
   */
  public function cvPageThree(array &$form, FormStateInterface $form_state)
  {
    $num_exp = $form_state->get('num_exp');
    if ($num_exp === NULL) {
      $num_exp = 1;
      $form_state->set('num_exp', 1);

    }

    $form['#tree'] = TRUE;
    $form['experience_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Experience'),
      '#prefix' => '<div id="experience-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_exp; $i++) {
      $form['experience_fieldset'][$i]['job_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Job title'),
      ];
      $form['experience_fieldset'][$i]['company'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Company name'),
      ];
      $form['experience_fieldset'][$i]['date_start'] = [
        '#type' => 'date',
        '#title' => $this->t('Start date'),
      ];
      $form['experience_fieldset'][$i]['date_end'] = [
        '#type' => 'date',
        '#title' => $this->t('End date'),
      ];
    }

    $form['experience_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['experience_fieldset']['actions']['add_exp'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'experience-fieldset-wrapper',
      ],
    ];

    if ($num_exp > 1) {
      $form['experience_fieldset']['actions']['remove_exp'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'experience-fieldset-wrapper',
        ],
      ];
    }

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#submit' => ['::nextStep'],
    );

    return $form;

  }

  /**
   * Callback function for multistep form cv_form.
   *
   * Callback for buildForm(), cvPageTwo(), cvPageThree().
   */
  public function nextStep(array &$form, FormStateInterface $form_state)
  {
    $step = $form_state->get('page_num') + 1;
    if ($step == 2) {
      if (!empty($form_state->getValue('image_photo'))) {
        $file = \Drupal::entityTypeManager()->getStorage('file')
          ->load($form_state->getValue('image_photo')[0]);
        $link = $file->url();
      } elseif (empty($form_state->getValue('image_photo'))) {
        $link = '';
        $file = NULL;
      }
      $country_code = $form_state->getValue('country');
      $country =  \Drupal::service('country_manager')->getList()[$country_code]->__toString();
      $form_state
        ->set('page_one_values', [
          'name' => $form_state->getValue('name'),
          'surname' => $form_state->getValue('surname'),
          'birthday' => $form_state->getValue('birthday'),
          'email' => $form_state->getValue('email'),
          'country' => $country,
          'city' => $form_state->getValue('city'),
          'address' => $form_state->getValue('address'),
          'image_photo' => $link,
          'image_file' => $file,
        ])
        ->set('page_num', $step)
        ->setRebuild(TRUE);

    }

    if ($step == 3) {
      $form_state
        ->set('page_two_values', [
          'skills' => $form_state->getValue('skills'),
        ])
        ->set('page_num', $step)
        ->setRebuild(TRUE);
    }

    if ($step == 4) {
      $form_state
        ->set('page_three_values', [
          'experience' => $form_state->getValue('experience_fieldset'),
        ])
        ->set('page_num', $step)
        ->setRebuild(TRUE);
    }

  }

  /**
   * Form constructor for the fourth step of multistep form cv_form.
   */
  public function cvPageFour(array &$form, FormStateInterface $form_state)
  {
    $page_one_values = $form_state->get('page_one_values');
    $page_two_values = $form_state->get('page_two_values');
    $page_three_values = $form_state->get('page_three_values');
    $exp_action_value = array_pop($page_three_values['experience']);
    $data['name'] = !empty($page_one_values['name']) ? $page_one_values['name'] : '';
    $data['surname'] = !empty($page_one_values['surname']) ? $page_one_values['surname'] : '';
    $data['birthday'] = !empty($page_one_values['birthday']) ? $page_one_values['birthday'] : '';
    $data['email'] = !empty($page_one_values['email']) ? $page_one_values['email'] : '';
    $data['country'] = !empty($page_one_values['country']) ? $page_one_values['country'] : '';
    $data['city'] = !empty($page_one_values['city']) ? $page_one_values['city'] : '';
    $data['address'] = !empty($page_one_values['address']) ? $page_one_values['address'] : '';
    $image_url = !empty($page_one_values['image_photo']) ? $page_one_values['image_photo'] : '';
    $data['skills'] = '';
    $empty_fields_count = 0;
    foreach ($page_three_values['experience']['0'] as $key => $value) {
      if (empty($value)) {
        $empty_fields_count++;
        if ($empty_fields_count === 4) {
          unset($page_three_values['experience']['0']);
        }
      }
    }
    $data['experience'] = !empty($page_three_values['experience']) ? $page_three_values['experience'] : array('no_exp' => 'without any experience');
    if (!empty($page_two_values['skills'])) {
      foreach ($page_two_values['skills'] as $skill) {
        $skill_name = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($skill['target_id'])->name->value;
        $data['skills'] .= $skill_name . ' ';
      }
    }
    $jobs_markup = '';
    foreach ($data['experience'] as $job) {
      if (isset($data['experience']['no_exp'])) {
        $jobs_markup = 'No exp';
      } else {
        $title = $job['job_title'] ? $job['job_title'] : 'No title';
        $company = $job['company'] ? $job['company'] : '? company';
        $start = $job['date_start'] ? $job['date_start'] : '?';
        $end = $job['date_end'] ? $job['date_end'] : '?';
        $jobs_markup .= '<p>' . $title . ' : ' . "($company)" . '<br>' . t('Job start: ') . $start . '<br>' . t('Date end: ') . $end . '</p>';
      }
    }
    $form['summary'] = array(
      '#type' => 'markup',
      '#markup' => Markup::create('<p>
        <table>
          <tr>
            <th>' . t('Name') . '</th>
            <th>' . t('Surname') . '</th>
            <th>' . t('Birthday') . '</th>
            <th>' . t('Country') . '</th>
            <th>' . t('City') . '</th>
            <th>' . t('Address') . '</th>
            <th>' . t('Link photo') . '</th>
          </tr>
          <tr>
            <td>' . $data['name'] . '</td>
            <td>' . $data['surname'] . '</td>
            <td>' . $data['birthday'] . '</td>
            <td>' . $data['country'] . '</td>
            <td>' . $data['city'] . '</td>
            <td>' . $data['address'] . '</td>
            <td>' . $image_url . '</td>
          </tr>
          <tr>
            <td>' . t('Skills') . '</td>
            <td colspan="6">' . $data['skills'] . '</td>
          </tr>
          </table>
    </p>' . '<h2>' . t('Experience') . '</h2>' . $jobs_markup),
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Send CV',
    );

    return $form;
  }

  /**
   * Callback for cvPageThree().
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state)
  {
    return $form['experience_fieldset'];
  }

  /**
   * Callback for cvPageThree().
   */
  public function addOne(array &$form, FormStateInterface $form_state)
  {
    $experience_fieldset = $form_state->get('num_exp');
    $add_button = $experience_fieldset + 1;
    $form_state->set('num_exp', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Callback for cvPageThree().
   */
  public function removeCallback(array &$form, FormStateInterface $form_state)
  {
    $exp_field = $form_state->get('num_exp');
    if ($exp_field > 1) {
      $remove_button = $exp_field - 1;
      $form_state->set('num_exp', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    if ($form_state->getValue('country') && empty($form_state->getValue('city'))) {
      $form_state->setErrorByName('city', $this->t('Please enter city'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $page_one_values = $form_state->get('page_one_values');
    $page_two_values = $form_state->get('page_two_values');
    $page_three_values = $form_state->get('page_three_values');
    $exp_action_value = array_pop($page_three_values['experience']);
    $params['name'] = !empty($page_one_values['name']) ? $page_one_values['name'] : '';
    $params['surname'] = !empty($page_one_values['surname']) ? $page_one_values['surname'] : '';
    $params['birthday'] = !empty($page_one_values['birthday']) ? $page_one_values['birthday'] : '';
    $params['email'] = !empty($page_one_values['email']) ? $page_one_values['email'] : '';
    $params['country'] = !empty($page_one_values['country']) ? $page_one_values['country'] : '';
    $params['city'] = !empty($page_one_values['city']) ? $page_one_values['city'] : '';
    $params['address'] = !empty($page_one_values['address']) ? $page_one_values['address'] : '';
    $params['image_photo'] = !empty($page_one_values['image_photo']) ? $page_one_values['image_photo'] : '';
    $params['image_file'] = is_object($page_one_values['image_file']) ? $page_one_values['image_file'] : '';
    $params['skills'] = '';
    $params['langcode'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $empty_fields_count = 0;
    foreach ($page_three_values['experience']['0'] as $key => $value) {
      if (empty($value)) {
        $empty_fields_count++;
        if ($empty_fields_count === 4) {
          unset($page_three_values['experience']['0']);
        }
      }
    }
    $params['experience'] = !empty($page_three_values['experience']) ? $page_three_values['experience'] : array('no_exp' => 'without any experience');
    $jobs_markup = '';
    foreach ($params['experience'] as $job) {
      if (isset($params['experience']['no_exp'])) {
        $jobs_markup = '<td><p>No exp</td></p>';
      } else {
        $title = $job['job_title'] ? $job['job_title'] : 'No title';
        $company = $job['company'] ? $job['company'] : '? company';
        $start = $job['date_start'] ? $job['date_start'] : '?';
        $end = $job['date_end'] ? $job['date_end'] : '?';
        $jobs_markup .= '<td><p>' . $title . '<br>' . $company . '<br>' . t('Job start: ') . $start . '<br>' . t('Date end: ') . $end . '</p></td>';
      }
    }
    $params['job_exp'] = $jobs_markup;
    if (!empty($page_two_values['skills'])) {
      foreach ($page_two_values['skills'] as $skill) {
        $skill_name = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($skill['target_id'])->name->value;
        $params['skills'] .= $skill_name . ' ';
      }
    }
    $mail_manager = \Drupal::service('plugin.manager.mail');
    $module = "cvs";
    $key = "cvs_submit_cvform_email_candidate";
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $to = $params['email'];
    $mail_manager->mail($module, $key, $langcode, $to, $params, NULL, TRUE);
    $key = "cvs_submit_cvform_email_admin";
    $mail_manager->mail($module, $key, $langcode, $to, $params, NULL, TRUE);
    $timestamp = \Drupal::time()->getCurrentTime();
    $time = \Drupal::service('date.formatter')->format($timestamp, 'custom', 'Y-m-d', NULL, $langcode);
    $field_cv = [
      'type' => 'cv',
      'langcode' => $langcode,
      'created' => $timestamp,
      'changed' => $timestamp,
      'moderation_state' => 'published',
      'title' => $params['name'] . ' ' . $params['surname'] . ' - ' . $time,
    ];
    if (!empty($params['name'])) {
      $field_cv['field_name'] = $params['name'];
    }
    if (!empty($params['surname'])) {
      $field_cv['field_surname'] = $params['surname'];
    }
    if (!empty($params['birthday'])) {
      $field_cv['field_birthday'] = $params['birthday'];
    }
    if (!empty($params['email'])) {
      $field_cv['field_email'] = $params['email'];
    }
    if (!empty($params['image_file'])) {
      $field_cv['field_photo'] = $params['image_file'];
    }
    if (!empty($params['country'])) {
      $field_cv['field_country'] = $params['country'];
    }
    if (!empty($params['city'])) {
      $field_cv['field_city'] = $params['city'];
    }
    if (!empty($params['address'])) {
      $field_cv['field_address'] = $params['address'];
    }
    $cv = Node::create($field_cv);
    if (!empty($params['skills'])) {
      foreach ($page_two_values['skills'] as $skill_term) {
        $cv->get('field_skills')->appendItem($skill_term);
      }
    }
    if (isset($params['experience'])) {
      if (!isset($params['experience']['no_exp'])) {
        foreach ($params['experience'] as $job) {
          $paragraph = Paragraph::create(['type' => 'experience']);
          $paragraph->set('field_job_title', $job['job_title']);
          $paragraph->set('field_company', $job['company']);
          $paragraph->set('field_date_start', $job['date_start']);
          $paragraph->set('field_date_end', $job['date_end']);
          $paragraph->isNew();
          $paragraph->save();
          $paragraph_data[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
        $cv->set('field_experience', $paragraph_data);
      }
    }
    $cv->save();
    $url = Url::fromRoute('entity.node.canonical', ['node' => $cv->id()], ['absolute' => TRUE]);
    $response = new RedirectResponse($url->toString());
    $response->send();
    \Drupal::messenger()->addMessage('Thx! Here is your CV');
  }

}

